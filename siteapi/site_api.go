package main

import (
	//"crypto/sha1"
	//"crypto/sha256"

	"database/sql"
	"time"
	//"encoding/base32"
	//"encoding/base64"
	"errors"
	"fmt"
	//"log"
	//"math/rand"
	"net/http"
	"os"
	"strconv"
	"strings"
	//"time"

	"github.com/gorilla/mux"
	//otp "github.com/hgfischer/go-otp"
	_ "github.com/lib/pq" // used for postgreSQL
	"github.com/mtmoses/color"
	"github.com/subosito/gotenv"
	//"golang.org/x/crypto/scrypt"

	"github.com/gorilla/rpc"
	"github.com/gorilla/rpc/json"
)

/*var sess *session.Session*/
var db *sql.DB
var err error

const _ver = "0.0.1"

/*
User is the core object
*/
type User struct {
	DBId       string            `json:"userid,omitempty"`
	Email      string            `json:"email,omitempty"`
	Password   string            `json:"password,omitempty"`
	Attributes map[string]string `json:"attributes,omitempty"`
	Created    time.Time         `json:"created,omitempty"`
	Modified   time.Time         `json:"modified,omitempty"`
}

/*
LogIn is for standard user login
- email (string)
- password (string)
*/
func (user *User) LogIn(r *http.Request, args *User, reply *string) error {
	fmt.Println(args)
	sqlStmt := fmt.Sprintf("select shard_1.checkuser('%s','%s')", args.Email, args.Password)
	fmt.Println(sqlStmt)
	rows, err := db.Query(sqlStmt)
	if err != nil {
		*reply = "-1"
		fmt.Println(err)
		return nil
	}
	defer rows.Close()
	for rows.Next() {
		var vCnt sql.NullString
		err := rows.Scan(&vCnt)
		if err != nil {
			*reply = "-2"
			fmt.Println(err)
			return nil
		}
		//fmt.Println("----------")
		//fmt.Println(vCnt.String)
		*reply = string(vCnt.String)
		return nil
		/*if vCnt.Int64 == 1 {
			*reply = "1"
		}*/
	}
	*reply = "-100"
	return nil
}

func main() {
	/*Loading Env variables*/
	gotenv.Load()
	showSplash()
	port := 9090 //os.Getenv("WEBSITE_PORT")
	hport := 9016
	url := "0.0.0.0"
	portEnv := os.Getenv("USER_PORT")
	urlEnv := os.Getenv("USER_URL")
	dbURLEnv := os.Getenv("USER_DB_URL")
	dbUserEnv := os.Getenv("USER_DB_USER")
	dbPassEnv := os.Getenv("USER_DB_PASS")
	dbNameEnv := os.Getenv("USER_DB_NAME")

	// start the database connection
	err := initDBConnection(dbUserEnv, dbPassEnv, dbURLEnv, dbNameEnv)
	if err != nil {
		panic(fmt.Sprintf("Error: %s\n", err.Error()))
	}

	// set the port and url for the http interface
	if len(portEnv) > 0 {
		i, err := strconv.Atoi(portEnv)
		if err != nil {
			panic("invalid port")
		}
		port = i
	}

	if len(urlEnv) > 0 {
		url = urlEnv
	}
	/*Starting Server*/
	startTCPService(url, port, hport)
}

func initDBConnection(dbUser, dbPass, dbURL, dbNAME string) (err error) {

	var user, pass, url, name string

	if len(dbUser) == 0 || len(dbURL) == 0 || len(dbPass) == 0 || len(dbNAME) == 0 {
		err = errors.New("Missing DB Credentails")
		return
	}

	if len(dbUser) > 0 && len(dbPass) > 0 && len(dbURL) > 0 && len(dbNAME) > 0 {
		user = strings.TrimSpace(dbUser)
		pass = strings.TrimSpace(dbPass)
		url = strings.TrimSpace(dbURL)
		name = strings.TrimSpace(dbNAME)
	}

	connString := fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=require", user, pass, url, name)
	fmt.Println(connString)
	fmt.Printf("connecting to database: %s\n", url)

	db, err = sql.Open("postgres", connString)
	if err != nil {
		err = fmt.Errorf("Database refused connection: %s", err.Error())
		return
	}

	fmt.Println("Db Connected")

	// set max connections
	db.SetMaxOpenConns(5)
	db.SetMaxIdleConns(5)

	return
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	screenImage := `
	888     888
	888     888
	888     888
	888     888 .d8888b   .d88b.  888d888
	888     888 88K      d8P  Y8b 888P"
	888     888 "Y8888b. 88888888 888
	Y88b. .d88P      X88 Y8b.     888
	 "Y88888P"   88888P'  "Y8888  888
	
	 ======================================
		HEALTHY MICROSERVICE
	 ======================================
	
	 `

	fmt.Fprintln(w, screenImage)

}

func showSplash() {
	screenImage := `
	888     888
	888     888
	888     888
	888     888 .d8888b   .d88b.  888d888
	888     888 88K      d8P  Y8b 888P"
	888     888 "Y8888b. 88888888 888
	Y88b. .d88P      X88 Y8b.     888
	 "Y88888P"   88888P'  "Y8888  888
	`

	c := color.New(color.FgBlue)
	c.Printf("%s\n\n", screenImage)
	fmt.Printf("User Microservice (%s)\n", _ver)
	fmt.Println("=================================")
}

func startTCPService(url string, port int, hport int) {

	if len(url) == 0 || port == 0 || hport == 0 {
		panic("no url to listen on")
	}

	fullURL := fmt.Sprintf(":%d", port)
	fmt.Printf("starting server on %s\n", fullURL)

	s := rpc.NewServer()
	s.RegisterCodec(json.NewCodec(), "application/json")
	s.RegisterCodec(json.NewCodec(), "application/json;charset=UTF-8")

	s.RegisterService(new(User), "")
	r := mux.NewRouter()
	r.Handle("/rpc", s)
	r.HandleFunc("/", indexHandler)
	http.ListenAndServe(fullURL, r)

}
