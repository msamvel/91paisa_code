$(document).ready(function() { 
    $("#frmlogin").validate({
        rules: { 
            username: {
                required: true,
                email: true
            },
            password: "required"
        },
        messages: { 
            username: {
                required: "Please enter your username",
                email: "Please enter a valid email address"
            },
            password: "Please enter your password"
        }
    })
    if ($("#ischecked").val() == "true")
    {
        $("#chkrememberme").prop("checked","true");
    }
});
