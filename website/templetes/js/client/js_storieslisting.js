$(document).ready(function() { 
    $(".clspublish").click(function() {
        $("#rowid").val($(this).attr("data-custom-value"));
        $("#actiontype").val("publish");
        $("#frmstoryobj").submit();
    });
    $(".clsdelete").click(function() {
        $("#rowid").val($(this).attr("data-custom-value"));
        $("#actiontype").val("delete");
        $("#frmstoryobj").submit();
    });
    $(".clsupvote").click(function() {
        $("#rowid").val($(this).attr("data-custom-value"));
        $("#actiontype").val("upvote");
        $("#frmstoryobj").submit();
    });
    $(".clsedit").click(function() {
        $("#rowid").val($(this).attr("data-custom-value"));
        $("#actiontype").val("upvote");
        $("#frmstoryobj").attr("action","/user/storiesedit");
        $("#frmstoryobj").submit();
    });
});
