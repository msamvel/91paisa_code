package main

import (
	"bytes"
	"database/sql"
	"errors"
	"fmt"
	"strings"
	"time"
	/*"io"*/
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	v "github.com/gorilla/rpc/json"
	_ "github.com/lib/pq"
	"github.com/mtmoses/httprouter"
	"github.com/subosito/gotenv"
)

var db *sql.DB
var tpl *template.Template
var currentLoginId string

/*var sess *session.Session*/
var err error
var r *gin.Engine
var rpcurl string

/*
LogIn is for standard user login
- email (string)
- password (string)
*/
type Stories struct {
	DBId       string            `json:"DBId,omitempty"`
	Stories    string            `json:"stories,omitempty"`
	Userid     string            `json:"userid,omitempty"`
	Attributes map[string]string `json:"attributes,omitempty"`
	Created    time.Time         `json:"created,omitempty"`
	Modified   time.Time         `json:"modified,omitempty"`
}

/*
User is the core object
*/
type User struct {
	DBId       string            `json:"userid,omitempty"`
	Email      string            `json:"email,omitempty"`
	Password   string            `json:"password,omitempty"`
	Attributes map[string]string `json:"attributes,omitempty"`
	Created    time.Time         `json:"created,omitempty"`
	Modified   time.Time         `json:"modified,omitempty"`
}

func render(c *gin.Context, data gin.H, templateName string) {
	switch c.Request.Header.Get("Accept") {
	case "application/json":
		c.JSON(http.StatusOK, data["payload"])
	case "application/xml":
		c.XML(http.StatusOK, data["payload"])
	default:
		c.HTML(http.StatusOK, templateName, data)
	}
}

func initializeRoutes(port string) {
	//port := ":" + os.Getenv("WEBSITE_PORT")
	fmt.Println(port)
	//url := os.Getenv("WEBSITE_URL")

	router := httprouter.New()
	r.GET("/test", showHomePage)
	r.GET("/", showHomePage)
	userRoutes := r.Group("/user")
	{
		userRoutes.GET("/", showHomePage)
		userRoutes.GET("/login", showLoginPage)
		userRoutes.POST("/login", performLogin)
		userRoutes.POST("/storiespublish", performStoriesPublish)
		userRoutes.POST("/storiesedit", performStoriesEdit)
		userRoutes.GET("/storiesadd", showStoriesAdd)
		userRoutes.POST("/storiesadd", performStoriesAdd)
		//userRoutes.POST("/storiesupvote", performStoriesUpvote)
	}
	fmt.Println("-------Starting server-------------")
	err := http.ListenAndServe(port, router)
	fmt.Println("Server Started at " + port)
	log.Println("Running api server in dev mode")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("Exiting Ser.....")
}

func initDBConnection(dbUser, dbPass, dbURL, dbNAME string) (err error) {

	var user, pass, url, name string

	if len(dbUser) == 0 || len(dbURL) == 0 || len(dbPass) == 0 || len(dbNAME) == 0 {
		err = errors.New("Missing DB Credentails")
		return
	}

	if len(dbUser) > 0 && len(dbPass) > 0 && len(dbURL) > 0 && len(dbNAME) > 0 {
		user = strings.TrimSpace(dbUser)
		pass = strings.TrimSpace(dbPass)
		url = strings.TrimSpace(dbURL)
		name = strings.TrimSpace(dbNAME)
	}

	connString := fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=require", user, pass, url, name)
	fmt.Println(connString)
	fmt.Printf("connecting to database: %s\n", url)

	db, err = sql.Open("postgres", connString)
	if err != nil {
		err = fmt.Errorf("Database refused connection: %s", err.Error())
		return
	}

	fmt.Println("Db Connected")

	// set max connections
	db.SetMaxOpenConns(5)
	db.SetMaxIdleConns(5)

	return
}

func main() {
	/*Loading Env variables*/
	gotenv.Load()
	port := ":" + os.Getenv("WEBSITE_PORT")
	dbURLEnv := os.Getenv("USER_DB_URL")
	dbUserEnv := os.Getenv("USER_DB_USER")
	dbPassEnv := os.Getenv("USER_DB_PASS")
	dbNameEnv := os.Getenv("USER_DB_NAME")

	/*Database*/
	err := initDBConnection(dbUserEnv, dbPassEnv, dbURLEnv, dbNameEnv)
	if err != nil {
		panic(fmt.Sprintf("Error: %s\n", err.Error()))
	}

	/*API*/
	rpcurl = "http://localhost:9090/rpc"

	/*Starting Server*/
	gin.SetMode(gin.ReleaseMode)
	gin.DefaultWriter = ioutil.Discard

	r = gin.Default()
	r.LoadHTMLGlob("templetes/html/*")
	r.Static("/css", "templetes/css")
	r.Static("/js", "templetes/js")
	r.Static("/img", "templetes/img")
	r.Static("/fonts", "templetes/fonts")
	go initializeRoutes(port)
	fmt.Println("Web Portal is running on :: " + port)
	r.Run(port)
	//r.Run()
}

/*func showHomePage(c *gin.Context) {
	c.JSON(200, gin.H{
		"message": "pong",
	})
}*/
func showLoginPage(c *gin.Context) {
	render(c, gin.H{
		"title": "Login",
	}, "login.html")
}
func performLogin_old(c *gin.Context) {
	username := c.PostForm("txtusername")
	password := c.PostForm("txtpassword")
	fmt.Println(username + " : " + password)
	sqlStmt := fmt.Sprintf("select shard_1.checkuser('%s','%s')", username, password)
	//sqlStmt := fmt.Sprintf("select count(*) as cnt from shard_1.user_tmp where email = '%s' and password = md5('%s')", username, password)
	rows, err := db.Query(sqlStmt)
	if err != nil {
		fmt.Println(err)
	}
	defer rows.Close()
	for rows.Next() {
		var vCnt sql.NullInt64
		err := rows.Scan(&vCnt)
		if err != nil {
			fmt.Println(err)
		}
		if vCnt.Int64 == 0 {

		}
	}
}

func performLogin(c *gin.Context) {
	username := c.PostForm("txtusername")
	password := c.PostForm("txtpassword")
	args := User{
		Email:    username,
		Password: password,
	}
	message, err := v.EncodeClientRequest("User.LogIn", args)
	if err != nil {
		fmt.Println(err)
	}

	req, err := http.NewRequest("POST", rpcurl, bytes.NewBuffer(message))
	fmt.Println(err)
	if err != nil {
		fmt.Println("b")
		fmt.Println(err)
	}

	req.Header.Set("Content-Type", "application/json")
	client := new(http.Client)
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("c")
		fmt.Println(err)
	}

	defer resp.Body.Close()
	var msg string
	err = v.DecodeClientResponse(resp.Body, &msg)
	if err != nil {
		fmt.Println("c")
		fmt.Println(err)
	}
	fmt.Println("-----------------")
	fmt.Println(msg)
	if msg > "0" {
		currentLoginId = msg //"1781476102225528117"
		showHomePage(c)
	} else {
		render(c, gin.H{
			"title":   "Login",
			"message": "Invalid",
		}, "login.html")
	}

}

/*
	GetStories
	if Param is the priamry key for edit
*/
func Getstories(Param string) (C []Stories, err error) {
	var sqlStmt string
	sqlStmt = fmt.Sprintf(`select s.id,stories,userid,u.attributes->'UserName' from shard_1.userstories_tmp s inner join shard_1.user_tmp u on s.userid = u.id`)
	if Param != "0" {
		sqlStmt = fmt.Sprintf(`%s where s.id = %s`, sqlStmt, Param)
	}
	fmt.Println(sqlStmt)
	rows, _ := db.Query(sqlStmt)

	defer rows.Close()
	for rows.Next() {
		var vID, vStories, vUserID, vUserName sql.NullString
		_ = rows.Scan(&vID, &vStories, &vUserID, &vUserName)
		attr := make(map[string]string)
		attr["UserName"] = vUserID.String
		attr["CurUser"] = currentLoginId
		story := Stories{DBId: vID.String, Stories: vStories.String, Userid: vUserID.String, Attributes: attr}
		fmt.Println(attr)
		C = append(C, story)
	}
	//fmt.Println(C)
	//fmt.Println("--------------------------")
	return
}

/*
	This will show listing
*/
func showHomePage(c *gin.Context) {
	storiesdetails, _ := Getstories("0")
	//fmt.Println(storiesdetails)
	render(c, gin.H{"payload": storiesdetails, "currentLoginId": currentLoginId}, "storieslisting.html")
}

/*
	This will perform public delete and upvote
*/
func performStoriesPublish(c *gin.Context) {
	rowid := c.PostForm("rowid")
	actiontype := c.PostForm("actiontype")
	processactions(rowid, actiontype)
	showHomePage(c)
}

/*
	This will perform edit
*/
func performStoriesEdit(c *gin.Context) {
	rowid := c.PostForm("rowid")
	storiesdetails, _ := Getstories(rowid)
	render(c, gin.H{"Stories": (storiesdetails[0]).Stories, "currentLoginId": currentLoginId, "storyid": rowid}, "storiesadd.html")
}

/*
	This will perform add stories
*/
func showStoriesAdd(c *gin.Context) {
	storiesdetails := ""
	fmt.Println(currentLoginId)
	render(c, gin.H{"payload": storiesdetails, "currentLoginId": currentLoginId, "storyid": 0}, "storiesadd.html")
}

/*
	This will perform save stories
*/
func performStoriesAdd(c *gin.Context) {
	storycontent := c.PostForm("txtstory")
	hdnid := c.PostForm("hdnid")
	hdnuserid := c.PostForm("hdnuserid")
	fmt.Println(storycontent)
	fmt.Println(hdnid)
	fmt.Println(hdnuserid)
	sqlStmt := fmt.Sprintf("select shard_1.AddEditStory('%s','%s','%s')", hdnid, storycontent, hdnuserid)
	fmt.Println(sqlStmt)
	stst, err := db.Exec(sqlStmt)
	fmt.Println(err)
	fmt.Println(stst)
	showHomePage(c)
}

/*
	This will perform calling store procedure to update status and increase upvote
*/
func processactions(rowid string, actiontype string) {
	sqlStmt := fmt.Sprintf("select shard_1.ProcessStory('%s','%s')", rowid, actiontype)
	fmt.Println(sqlStmt)
	stst, err := db.Exec(sqlStmt)
	fmt.Println(err)
	fmt.Println(stst)
}
